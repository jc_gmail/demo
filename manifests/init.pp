# Class:  demomodule 
# ===========================
#
# Insert description of module here
#
# === Parameters
#
# @param package_name Name of package to be installed
#  Insert description here
#  Default: ''
#
# @param service_name Name of service to be installed
#  Insert description here
#  Default: ''

class demomodule (
  String $package_name,
  String $service_name,
) {

  # validate parameters here
  assert_type(String, $package_name) | $expected, $actual  | {
    fail("The package_name must be a String - This was set to:  ${actual}")
  }

  assert_type(String, $service_name) | $expected, $actual  | {
    fail("The service_name must be a String - This was set to:  ${actual}")
  }

  case $facts['os']['family'] {
    'windows': {
      class { '::demomodule::install::windows':
        package_name => $package_name,
        service_name => $service_name,
      }
      contain '::demomodule::install::windows'
    }
    'RedHat', 'CentOS': {
      class { '::demomodule::install::linux':
        package_name => $package_name,
        service_name => $service_name,
      }
      contain '::demomodule::install::linux'
    }
    default : { fail("Unsupported OS - ${facts['os']['family']}")}
  }

}
