# Class: demomodule::install::windows
# ===========================
#
# This class is called from demomodule for install.
#
# Insert description here
#
# === Parameters
#
# @param package_name Name of package to be installed
#  Insert description here
#  Default: ''
#
# @param service_name Name of service to be installed
#  Insert description here
#  Default: ''

class demomodule::install::windows(
  String $package_name,
  String $service_name,
  ) {

  if ! defined (Class['::demomodule']){
    fail ('You must include the ::demomodule class before use demomodule::install::windows')
  }

  package { $package_name:
    ensure => present,
  }
}
