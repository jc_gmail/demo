require 'spec_helper_acceptance'

describe 'demomodule' do
  context 'demomodule should be applied' do
    it 'Applies demomodule' do
      pp = <<-PP
        include ::demomodule
      PP

      apply_manifest(pp, catch_failures: true)
      apply_manifest(pp, catch_failures: true)
    end
  end
end

if os[:family] == 'windows'
  # Write Windows tests here
elsif ['redhat', 'ubuntu'].include?(os[:family])
  # Write Linux tests here
end

=begin
Examples

if os[:family] == 'windows'

  describe windows_registry_key('HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System') do
    it { should have_property_value('EnableLUA', :type_dword, '0x00000000') }
  end

  describe windows_registry_key('HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System') do
    it { should have_property_value('ConsentPromptBehaviorAdmin', :type_dword, '0x00000000') }
  end

end
=end
